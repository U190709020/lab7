public class MyDate {
    int day;
    int month;
    int year;

    public MyDate(int day, int month, int year) {
        this.day = day;
        this.month = month;
        this.year = year;
    }

    public String toString() {
        return this.year + "-" + this.month + "-" + this.day;
    }

    int[] MONTHS = {31,28,31,30,31,30,31,31,30,31,30,31};

    public boolean leap(){
        return year % 4 == 0;
    }

    public void incrementDay() {
        if((this.day + 1) > MONTHS[month-1]){
            if(this.month == 2 && leap()) {
                this.day = 29;
            }else{
                this.day =1;
                this.month+=1;
            }
        }else{
            this.day +=1;
        }
    }

    public void incrementDay(int PLUSDAY) {
        if((this.day + PLUSDAY) > MONTHS[month-1]){
            this.day += (PLUSDAY - (MONTHS[month-1] - this.day));
            this.month += 1;
        }else{
            this.day += PLUSDAY;
        }
    }

    public void decrementDay() {
        if(this.day == 1){
            this.month -= 1;
            if ( this.month == 2 && leap()){
                this.day = 29;
            }else{
                this.day = MONTHS[month-1];
            }
        }else{
            this.day -= 1;
        }
    }

    public void decrementDay(int day) {
        if(this.day - day <= 0){
            this.month -= 1;
            this.day -= day;
            if(this.day ==0){
                this.day = MONTHS[month-1];
            }else{
                this.day = this.day + MONTHS[month-1];
            }

        }else{
            this.day -= day;
        }

    }

    public void incrementMonth() {
        if(this.month == 12 ){
            this.year+=1;
            this.month = 1;
        }else{
            this.month+=1;
        }
    }

    public void incrementMonth(int PLUSMONTH) {
        if(this.month + PLUSMONTH > 12){
            this.year += (this.month + PLUSMONTH) % 12 ;
            this.month = (this.month + PLUSMONTH) - (((this.month + PLUSMONTH) % 12) * 12);
        }else{
            this.month += PLUSMONTH;
        }
        if(this.day>MONTHS[month-1]){
            if (this.month == 2 && leap()){
                this.day = 29;
            }else {
                this.day = MONTHS[month - 1];
            }
        }
    }

    public void decrementMonth() {
        if(this.month == 1){
            this.year --;
            this.month = 12;
        }else{
            this.month --;
        }
    }

    public void decrementMonth(int monthOff) {
        this.month -= monthOff;
        if(this.day<=0) {
            this.day = MONTHS[month-1] + this.day;
            this.month --;
            while (this.day <= 0){
                this.day = MONTHS[month-1]+this.day;
                this.month --;
            }
        }
        if(this.month <=0 ){
            this.month = 12 + this.month;
            this.year --;
            while (this.month <= 0){
                this.month = 12 + this.month;
                this.year --;

            }
        }if(this.day>MONTHS[month-1]){
            this.day --;
        }

    }

    public void incrementYear() {
        this.year ++;
    }

    public void incrementYear(int addedYear) {
        this.year += addedYear;
    }

    public void decrementYear() {
        if(leap() && this.month == 2){
            this.year --;
            this.day --;
        }
        else{
            this.year --;
        }

    }

    public void decrementYear(int yearOff) {
        this.year = this.year - yearOff;
    }

    public boolean isBefore(MyDate anotherDate){
        if(this.year > anotherDate.year){
            return false;
        }
        if(this.year == anotherDate.year && this.month> anotherDate.month){
            return false;
        }
        if(this.year == anotherDate.year && this.month == anotherDate.month && this.day> anotherDate.day){
            return false;
        }
        if(this.year == anotherDate.year && this.month == anotherDate.month && this.day == anotherDate.day){
            return false;
        }
        else{
            return true;
        }
    }

    public boolean isAfter(MyDate anotherDate){
        if(this.year>anotherDate.year){
            return true;
        }
        if (this.year == anotherDate.year && this.month>anotherDate.month){
            return true;
        }
        if (this.year == anotherDate.year && this.month > anotherDate.month && this.day > anotherDate.day){

            return true;
        }

        else {
            return false;
        }
    }

    public int dayDifference(MyDate anotherDate){
        int Years = (this.year -anotherDate.year) * 365;
        int Months = (this.month - anotherDate.month) * 30;
        int Days = (this.day - anotherDate.day);
        return Math.abs(Days+  Months + Years);
    }

}
